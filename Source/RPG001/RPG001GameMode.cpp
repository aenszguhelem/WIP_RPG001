// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "RPG001GameMode.h"
#include "RPG001Character.h"
#include "UObject/ConstructorHelpers.h"

ARPG001GameMode::ARPG001GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
