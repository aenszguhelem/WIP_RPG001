// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RPG001GameMode.generated.h"

UCLASS(minimalapi)
class ARPG001GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARPG001GameMode();
};



